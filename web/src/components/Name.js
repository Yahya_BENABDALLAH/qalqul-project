import React, { useState } from "react";
import Chat from "./Chat";
import "./style/style.css";

export const Name = () => {
  const [name, setName] = useState();
  const [X, setX] = useState(false);

  const push = () => {
    setX(true);
  };
  return (
    <>
      {!X ? (
        <div className="field">
          <input
          className="field__input"
            type="text"
            required
            onChange={(e) => setName(e.target.value)}
          />
          <button onClick={() => push()}>Chat !</button>{" "}
        </div>
      ) : (
        ""
      )}
      {X ? <Chat Name={name} /> : ""}
    </>
  );
};
