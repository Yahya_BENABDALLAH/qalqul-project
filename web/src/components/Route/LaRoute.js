import React from "react";
import { Switch, Route } from "react-router-dom";
import Chat from "../Chat";
import {Name} from "../Name";


const LaRoute = () => {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={Name} />
        <Route exact path="/chat" component={Chat} />
      </Switch>
    </div>
  );
};

export default LaRoute;
