import React, { useEffect, useState } from "react";
import "./style/style.css";
import SendIcon from "@material-ui/icons/Send";
import io from "socket.io-client";

const socket = io("http://localhost:5000");

const Chat = ({ Name }) => {
  const [sms, setSms] = useState([]);
  const [message, setMessage] = useState();

  useEffect(() => {
    socket.on("messageAll", (message) => {
      setSms([message, ...sms]);
    });
    return () => {};
  }, [sms]); 

  const envoyerMessage = (message) => {
    console.log({ id: socket.id, name: Name, Temps: Date.now(), msg: message });
    document.querySelector(".inputMessage").value = "";
    socket.emit("messageClient", {
      id: socket.id,
      name: Name,
      Temps: Date.now(),
      msg: message,
    });
  };

  const Enter = (e) => {
    if (e.keyCode === 13) envoyerMessage(message);
  };

  return (
    <div className="divGlobale">
      <div className="divReverse">
        {sms ? (
          <>
            {sms.map((e) => {
              return (
                <div
                  className={
                    socket.id === e.id ? "Users divUser" : "Users"
                  }
                  key={e.Temps * Math.random()}
                >
                  <strong
                    style={
                      socket.id === e.socket_id
                        ? { color: "#3a5099" }
                        : { color: "#216939" }
                    }
                  >
                    {console.log(socket.id)}
                    {/* {console.log(e.socket_id)} */}
                    {e.name}
                  </strong>{" "}
                  <span
                    className={
                      socket.id === e.id ? "TextRight" : "TextLeft"
                    }
                  >
                    {e.msg}{" "}
                  </span>
                  <span
                    className={
                      socket.id === e.socket_id ? "TempsRight" : "TempsLeft"
                    }
                  >
                    {new Date(Number(e.Temps)).toString().substring(16, 21)}
                  </span>
                </div>
              );
            })}
          </>
        ) : (
          ""
        )}
      </div>
      <div className="input-container">
        <input
          onKeyUp={(e) => Enter(e)}
          className="inputMessage"
          type="text"
          onChange={(e) => setMessage(e.target.value)}
          placeholder="  Ecrivez votre message ..."
          style={{ backgroundColor: "#ffff" }}
        />
        <button className="btnMessage" onClick={() => envoyerMessage(message)}>
          <SendIcon style={{ color: "green" }}></SendIcon>
        </button>
      </div>
    </div>
  );
};;

export default Chat;
