import LaRoute from './components/Route/LaRoute';

function App() {
  return (
    <div className="App">
        <LaRoute/>
    </div>
  );
}

export default App;
