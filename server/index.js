const app = require("express")();
const server = require("http").createServer(app);
const io = require("socket.io")(server, {
  // Cors est obligé pour authorisee cross-origin requests
  cors: {
    origin: ["http://localhost:3000"],
  },
});

io.on("connection", (socket) => {
  // on ouvre notre connection
  socket.on("messageClient", (sms) => {
    console.log(sms), // on recoit le message dans la console
      socket.broadcast.emit("messageAll", sms), // on envoie le message à tous les clients connectés
      socket.emit("messageAll", sms); // on envoie le message à l'emetteur
  });
});

server.listen(5000, () => console.log("Server a demarer dans le port 5000"));
